object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 443
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormInit
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object player1Label: TLabel
    Left = 8
    Top = 96
    Width = 95
    Height = 24
    Caption = 'Joueur 1 : '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object player2Label: TLabel
    Left = 592
    Top = 96
    Width = 89
    Height = 24
    Caption = ': Joueur 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object player1CardLabel: TLabel
    Left = 109
    Top = 96
    Width = 6
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object player2CardLabel: TLabel
    Left = 469
    Top = 96
    Width = 6
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object resultLabel: TLabel
    Left = 264
    Top = 24
    Width = 222
    Height = 24
    Caption = 'La partie va commencer!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object player1NbCards: TLabel
    Left = 8
    Top = 136
    Width = 6
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object player2NbCards: TLabel
    Left = 596
    Top = 126
    Width = 6
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -20
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object playBtn: TButton
    Left = 304
    Top = 288
    Width = 105
    Height = 57
    Caption = 'Jouer'
    TabOrder = 0
    OnClick = playBtnClick
  end
end
