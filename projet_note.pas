unit projet_note;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, math, System.StrUtils, System.Types,
  Vcl.ExtCtrls, System.ImageList, Vcl.ImgList;

type
  TForm2 = class(TForm)
    player1Label: TLabel;
    player2Label: TLabel;
    player1CardLabel: TLabel;
    player2CardLabel: TLabel;
    resultLabel: TLabel;
    playBtn: TButton;
    player1NbCards: TLabel;
    player2NbCards: TLabel;
  procedure playBtnClick(Sender: TObject);
  procedure FormInit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    stackSize : integer;
    //cardData : array of string;
  public
    { Public declarations }
    procedure makeGame();
    procedure randomize();
    procedure distribute();
    procedure switch(var winner : TStringList; var loser : TStringList);
    procedure gameWin();
    procedure reset();
    procedure displayCardPlayed(var playerLabel : TLabel; var card : TStringDynArray);
    procedure displayImage(xOffset : integer; yOffset : integer; path : string);
  end;

var
  Form2: TForm2;
  player1, player2 : TStringList;
  cards : array[0..51] of string;

implementation

{$R *.dfm}

procedure TForm2.FormDestroy(Sender: TObject);
begin
  player1.Destroy;
  player2.Destroy;
end;

procedure TForm2.FormInit(Sender: TObject);
begin
  player1 := TStringList.Create;
  player2 := TStringList.Create;
  reset();
end;

procedure TForm2.reset;
begin
  makeGame();
  distribute();
end;

procedure TForm2.makeGame;
var
  sign : array[0..3] of string;
  types : array[0..13] of string;
  i, j : integer;
begin
  sign[0] := ' pique';
  sign[1] := ' coeur';
  sign[2] := ' carreau';
  sign[3] := ' trefle';

  types[0] := '1 de';
  types[1] := '2 de';
  types[2] := '3 de';
  types[3] := '4 de';
  types[4] := '5 de';
  types[5] := '6 de';
  types[6] := '7 de';
  types[7] := '8 de';
  types[8] := '9 de';
  types[9] := '10 de';
  types[10] := '11 de';
  types[11] := '12 de';
  types[12] := '13 de';

  for i := 0 to 3 do
  begin
    for j := 0 to 12 do
    begin
      cards[13 * i + j] := types[j]+ sign[i];
    end;
  end;
  randomize();
  stackSize:= 0;
end;

procedure TForm2.switch(var winner : TStringList; var loser : TStringList);
var
  i : integer;
begin
  for i := 0 to stackSize do
  begin
    winner.Add(loser[0]);
    winner.Add(winner[0]);
    loser.Delete(0);
    winner.Delete(0)
  end;
  stackSize := 0;

  player1NbCards.Caption := inttostr(player1.Count) + ' cartes';
  player2NbCards.Caption := inttostr(player2.Count) + ' cartes';
end;

procedure TForm2.displayCardPlayed(var playerLabel : TLabel; var card : TStringDynArray);
var
  cardValue : string;
begin
  if(card[0] = '1') then
  begin
    cardValue := 'as';
  end
  else if(card[0] = '11') then
  begin
    cardValue := 'valet';
  end
  else if(card[0] = '12') then
  begin
    cardValue := 'dame';
  end
  else if(card[0] = '13') then
  begin
    cardValue := 'roi';
  end
  else
    cardValue :=  card[0];
  playerLabel.Caption := cardValue + ' ' + card[1] + ' ' + card[2];
end;

procedure TForm2.playBtnClick(Sender: TObject);
var
  cardPlayer1, cardPlayer2 : TStringDynArray;
begin
  cardPlayer1 := SplitString(player1[stackSize], ' ');
  cardPlayer2 := SplitString(player2[stackSize], ' ');
  displayCardPlayed(player1CardLabel,cardPlayer1 );
  displayCardPlayed(player2CardLabel, cardPlayer2);
  if(strtoint(cardPlayer1[0]) > strtoint(cardPlayer2[0])) then
  begin
    switch(player1, player2);
    resultLabel.Caption := 'Joueur 1 gagne la manche!';

  end
  else if(strtoint(cardPlayer1[0]) < strtoint(cardPlayer2[0])) then
  begin
    switch(player2, player1);
     resultLabel.Caption := 'Joueur 2 gagne la manche!';
  end
  else
  begin
    stackSize := stackSize + 1;
     resultLabel.Caption := 'Egalit�!';
  end;
  gameWin();
  displayImage(100, 300, cardPlayer1[0] + cardPlayer1[2]);
  displayImage(800, 300, cardPlayer2[0] + cardPlayer2[2]);
end;


procedure TForm2.gameWin;
begin
  if(player1.Count - stackSize = 0) then
  begin
    resultLabel.Caption := 'Joueur2 a gagn�!';
    reset();
  end
  else if(player2.Count - stackSize = 0) then
  begin
    resultLabel.Caption := 'Joueur1 a gagn�!';
     reset();
  end;
end;

procedure TForm2.randomize;
var
  nbChange, i, origin, destination : integer;
  tmp : string;
begin
  nbChange :=  RandomRange(20, 100);
  for i := 0 to nbChange do
  begin
    origin := RandomRange(0, 51);
    destination := RandomRange(0, 51);
    tmp := cards[destination];
    cards[destination] := cards[origin];
    cards[origin] := tmp;
  end;
end;

procedure TForm2.distribute();
var
  i : integer;
begin
  player1.Clear;
  player2.Clear;

  for i := 0 to 25 do
  begin
    player1.Add(cards[i * 2]);
    player2.Add(cards[i * 2 + 1]);
  end;
end;

procedure TForm2.displayImage(xOffset : integer; yOffset : integer; path : string);
var
  Bitmap : TBitmap;
  MyRgn : HRGN;
begin
     Bitmap := TBitmap.Create;
      try
        Bitmap.LoadFromFile('res/' + path + '.bmp');
        Form2.Canvas.Brush.Bitmap := Bitmap;
        Form2.Canvas.Draw(xOffset,yOffset,BitMap);
      finally
        Form2.Canvas.Brush.Bitmap := nil;
        Bitmap.Destroy;
      end;
end;

end.
